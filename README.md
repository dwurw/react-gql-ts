# Portfolio app

## Requirements

-   yarn

## Available scripts

| Command                 | Description                            |
| ----------------------- | -------------------------------------- |
| `yarn`                  | Installs all dependencies              |
| `yarn build`            | Builds client and server app sides     |
| `yarn build:client`     | Builds FE app side                     |
| `yarn build:server`     | Builds server app side                 |
| `yarn start:client`     | Starts FE app on port 3000             |
| `yarn start:server`     | Starts BE app on port 4000             |
