import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components';
import App from './App';
import { GlobalStyle } from './styles/css/style';
import { theme } from './styles/theme';
import registerServiceWorker from './utils/registerServiceWorker';

const HTML_TARGET = document.getElementById('root') as HTMLElement;

ReactDOM.render(
    <>
        <GlobalStyle />
        <ThemeProvider theme={theme}>
            <App />
        </ThemeProvider>
    </>,
    HTML_TARGET,
);

registerServiceWorker();
