import styled from 'styled-components';
import { ITheme } from 'styles/theme';

export interface ISectionStyled {
    theme: ITheme;
}

/** Custom scroll bar */
export const SectionStyled = styled.div`
    height: 100vh;
    position: relative;
`;
