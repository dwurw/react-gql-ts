import { AnimationEnum } from 'components/Animation/AnimationEnum';
import { SectionStyled } from 'components/SectionStyled';
import * as React from 'react';
import { Animation } from '../Animation/Animation';
import { HeaderStyled } from './HeaderStyled';

/** The first page section */
const Header: React.SFC = () => (
    <SectionStyled>
        <HeaderStyled>
            <div className="header">
                <Animation width={55} contentType={AnimationEnum.Header} autoplay/>
            </div>
            <div className="header-footer">
                <div className="c-g">with</div>
                <Animation width={10} contentType={AnimationEnum.Heart} autoplay loop className="mt5" />
            </div>
        </HeaderStyled>
    </SectionStyled>
);

export default Header;
