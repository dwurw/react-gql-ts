import styled from 'styled-components';
import { ITheme } from '../../styles/theme';

export interface IHeaderStyle {
    theme: ITheme;
    end?: boolean;
}

/** Wrapper for Header */
export const HeaderStyled = styled.div`
    height: 100%;
    font-size: 6rem;
    display: flex;
    flex-direction: column;
    align-items: center;

    .header {
        margin-top: 8rem;
    }

    .header-footer {
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;
