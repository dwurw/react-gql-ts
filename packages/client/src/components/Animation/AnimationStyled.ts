import styled from "styled-components";
import { ITheme } from "../../styles/theme";

export interface IAnimationStyled {
  theme: ITheme;
  width: number;
  height?: number;
}

export const AnimationStyled = styled.div`
  background: transparent;
  width: ${(props: IAnimationStyled) => props.width}rem;
  height: ${(props: IAnimationStyled) => props.height}rem;
`;
