import { addAnimation } from "assets/addAnimation";
import { datePickerAnimation } from "assets/datePickerAnimation";
import { headerAnimation } from "assets/headerAnimation";
import { heartAnimation } from "assets/heartAnimation";
import { homeAnimation } from "assets/homeAnimation";
import { musicAnimation } from "assets/musicAnimation";
import { petAnimation } from "assets/petAnimation";
import { profileAnimation } from "assets/profileAnimation";

export enum AnimationEnum {
  Add = "ADD",
  DatePicker = "DATE_PICKER",
  Header = "HEADER",
  Heart = "HEART",
  Home = "HOME",
  Music = "MUSIC",
  Pet = "PET",
  Profile = "PROFILE"
}

export function getAnimation(animation: AnimationEnum): object {
  switch (animation) {
    case AnimationEnum.Add:
      return addAnimation;
    case AnimationEnum.Heart:
      return heartAnimation;
    case AnimationEnum.DatePicker:
      return datePickerAnimation;
    case AnimationEnum.Header:
      return headerAnimation;
    case AnimationEnum.Home:
      return homeAnimation;
    case AnimationEnum.Music:
      return musicAnimation;
    case AnimationEnum.Pet:
      return petAnimation;
    case AnimationEnum.Profile:
      return profileAnimation;
    default:
      throw new Error("Animation not supported");
  }
}
