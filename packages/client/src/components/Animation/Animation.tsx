import * as React from 'react';
import Lottie, { Options as LottieOptions } from 'react-lottie';
import { AnimationEnum, getAnimation } from './AnimationEnum';
import { AnimationStyled } from './AnimationStyled';

export interface IAnimation {
    /** Animation size in rems */
    width: number;
    /** if height will not be passed, width will be used */
    height?: number;
    /** Type of animation body */
    contentType: AnimationEnum;
    /** If true, animation will be played non-stop (default - false) */
    loop?: boolean;
    /** If true, animation will be after render (default - false) */
    autoplay?: boolean;
    /** Class name will be applied on parent div */
    className?: string;
    /** If true, animation will be paused after last frame */
    pause?: boolean;
}

/** Wrapper for React Lottie, provides animation */
export const Animation: React.SFC<IAnimation> = (props: IAnimation) => {
    /** Basic options */
    const lottieOptions: LottieOptions = {
        animationData: getAnimation(props.contentType),
        loop: props.loop || false,
        autoplay: props.autoplay || false,
    };

    return (
        <AnimationStyled className={props.className} width={props.width} height={props.height}>
            <Lottie options={lottieOptions} speed={2} isClickToPauseDisabled isPaused={props.pause} />
        </AnimationStyled>
    );
};
