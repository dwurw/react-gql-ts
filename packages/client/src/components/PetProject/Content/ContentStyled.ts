import { ITheme } from "styles/theme";
import styled from "styled-components";

export interface IContentStyled {
  theme: ITheme;
}

/**
 * Create an wrapper for Input
 */
export const ContentStyled = styled.div`
  height: 40%;
  color: ${({ theme }: IContentStyled) => theme.colors.gray};
  text-align: left;
`;
