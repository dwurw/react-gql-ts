import * as React from "react";
import { ContentStyled } from "./ContentStyled";

export const Content: React.SFC = () => {
  return (
    <ContentStyled>
      <ul>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
        <li>Test</li>
      </ul>
    </ContentStyled>
  );
};
