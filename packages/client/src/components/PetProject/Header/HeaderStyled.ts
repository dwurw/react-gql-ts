import { ITheme } from "styles/theme";
import styled from "styled-components";

export interface IHeaderStyled {
  theme: ITheme;
}

/**
 * Create an wrapper for Input
 */
export const HeaderStyled = styled.div`
  height: 15%;
  border-radius: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1.5rem;
  color: ${({ theme }: IHeaderStyled) => theme.colors.gray};
  background-color: ${({ theme }: IHeaderStyled) => theme.colors.red};
`;
