import * as React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { TabsStyled } from "./TabsStyled";

export interface IOptions {
  value: string;
  label: string;
}

export interface RadioProps {
  /** All radio group options */
  options: IOptions[];
  /** Called on value change */
  onChange: (value: string) => void;
  /** Value consist name of picked string */
  value: string;
}

export const MainCardHeaderTabs: React.SFC = props => {
  return (
    <TabsStyled>
      <Tabs>
        <TabList>
          <Tab>TODAY</Tab>
          <Tab>TOMORROW</Tab>
          <Tab>25.05.2019</Tab>
          <Tab>26.05.2019</Tab>
          <Tab>27.05.2019</Tab>
          <Tab>28.05.2019</Tab>
          <Tab>29.05.2019</Tab>
        </TabList>
      </Tabs>
    </TabsStyled>
  );
};
