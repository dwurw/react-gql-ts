import styled, { withTheme, css } from "styled-components";
import { ITheme } from "styles/theme";

export interface ITabsStyled {
  theme: ITheme;
}

/**
 * Create an RadioButton component
 */
export const TabsStyled = styled.div`
  color: ${({ theme }: ITabsStyled) => theme.colors.black};
  padding-left: 1.5rem;

  .react-tabs__tab--selected {
    background-color: transparent;
    border: 0;
    border-bottom: 10px solid ${({ theme }: ITabsStyled) => theme.colors.gray};
    border-radius: 40px;
    color: ${({ theme }: ITabsStyled) => theme.colors.gray};
  }

  .react-tabs__tab-list {
    border: 0;
  }
`;
