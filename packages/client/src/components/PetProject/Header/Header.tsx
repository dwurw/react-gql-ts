import * as React from "react";
import { HeaderStyled } from "./HeaderStyled";
import { MainCardHeaderTabs } from "./Tabs/Tabs";
import { DatePicker } from "./DatePicker/Datepicker";

export const Header: React.SFC = () => {
  return (
    <HeaderStyled>
      {/* <MainCardHeaderTabs /> */}
      <DatePicker/>
    </HeaderStyled>
  );
};
