import * as React from "react";
import { datePickerAnimation } from "assets/datePickerAnimation";
import Lottie from "react-lottie";


export const DatePicker: React.SFC = props => {
  return (
    <div style={{ width: "3rem", padding:'10px' }}>
    <Lottie options={{ animationData: datePickerAnimation }} />
  </div>
  );
};
