import * as React from "react";
import { ITheme } from "styles/theme";
import styled from "styled-components";
import Lottie from "react-lottie";
import { addAnimation } from "assets/addAnimation";
import { Input } from "./Input/Input";
import { FooterStyled } from "./FooterStyled";

export const Footer: React.SFC = () => {
  return (
    <FooterStyled>
      <Input />
      <div style={{ width: "3rem", padding:'10px' }}>
        <Lottie options={{ animationData: addAnimation }} />
      </div>
    </FooterStyled>
  );
};
