import { ITheme } from "styles/theme";
import styled from "styled-components";

export interface IFooterStyled {
  theme: ITheme;
}

/**
 * Create an wrapper for Input
 */
export const FooterStyled = styled.div`
  height: 15%;
  display: flex;
  border-radius: 40px;
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }: IFooterStyled) => theme.colors.gray};
`;
