import styled from "styled-components";
import { ITheme } from "../../../../styles/theme";

export interface IInputStyled {
  theme: ITheme;
}

/** Create an wrapper for Input */
export const InputStyled = styled.input`
  text-align: center;
  background: transparent;
  border: 0;
  outline: none;
  width: 100%;
  padding-left: 1.5rem;
  font-size: 1.5rem;
`;
