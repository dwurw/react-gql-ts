import * as React from "react";
import { InputStyled } from "./InputStyled";

export const Input: React.SFC = () => {
  return <InputStyled placeholder='Add your important todo!'/>;
};
