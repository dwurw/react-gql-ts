import { SectionStyled } from 'components/SectionStyled';
import * as React from 'react';
import { Content } from './Content/Content';
import { Footer } from './Footer/Footer';
import { Header } from './Header/Header';
import { PetProjectStyled } from './PetProjectStyled';

const PetProject: React.SFC = () => {
    return (
        <SectionStyled>
            <PetProjectStyled>
                <Header />
                <Content />
                <Footer />
            </PetProjectStyled>
        </SectionStyled>
    );
};

export default PetProject;
