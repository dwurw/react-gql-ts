import styled from 'styled-components';
import { ITheme } from 'styles/theme';

export interface IScrollStyled {
    theme: ITheme;
    percent: number;
}

/** Custom scroll bar */
export const ScrollStyled = styled.div`
    margin-top: -0.1rem;
    width: 0.5rem;
    /* FIXME: styled components warning */
    height: ${({ percent }: IScrollStyled) => percent * 0.135}rem;

    background-color: #f6e198;
    border-radius: 10px;
`;
