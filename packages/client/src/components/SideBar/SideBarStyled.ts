import styled from 'styled-components';
import { ITheme } from 'styles/theme';

export interface ISideBarStyled {
    theme: ITheme;
}

/** Styles for {@link SideBar} component */
export const SideBarStyled = styled.div`
    color: ${({ theme }: ISideBarStyled) => theme.colors.black};
    margin: 0.5rem;
    display: -webkit-box;

    position: fixed;
    right: 0;
    top: 50%;
    transform: translateY(-50%);

    ul {
        list-style: none;
        display: table-caption;
        margin: 0;
    }

    li {
        display: inline-block;
    }

    /* TODO: add responsive media queries */
`;
