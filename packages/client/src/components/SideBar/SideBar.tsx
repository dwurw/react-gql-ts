import { AnimationEnum } from 'components/Animation/AnimationEnum';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { ScrollStyled } from './ScrollStyled';
import { SideBarItem } from './SideBarItem';
import { SideBarStyled } from './SideBarStyled';

// FIXME: move to constants
const itemsCount = 3;
const defaultPercentage = Math.round(100 / itemsCount); // 33%

/**
 * Renders sidebar that allows to move faster between sections
 * Has custom scroll bar inside
 */
const SideBar: React.SFC = () => {
    /** Scroll event handlers */
    const [scrollPercent, setScrollPercent] = useState(defaultPercentage);

    useEffect(() => {
        // FIXME: move scroll to default to "componentWillMount"
        window.scrollTo(0, 0);

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    } /** FIXME: empty array is not safe */, []);

    /** Converting scroll pixel value to percentage (starts with defaultPercentage) */
    const handleScroll = () => {
        const scrollPercentRounded = (window.pageYOffset / document.body.offsetHeight) * 100 + defaultPercentage;
        setScrollPercent(scrollPercentRounded);
    };

    return (
        <SideBarStyled>
            <ul>
                <SideBarItem index={0} width={3} contentType={AnimationEnum.Home} scrollPercent={scrollPercent} />
                <SideBarItem
                    index={1}
                    width={3}
                    contentType={AnimationEnum.Profile}
                    scrollPercent={scrollPercent}
                    className="pt1 relative z1"
                />
                <SideBarItem index={2} width={7} contentType={AnimationEnum.Pet} scrollPercent={scrollPercent} className="z-1 static" />
            </ul>
            <ScrollStyled percent={scrollPercent} />
        </SideBarStyled>
    );
};

export default SideBar;
