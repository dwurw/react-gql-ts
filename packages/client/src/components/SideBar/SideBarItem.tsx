import * as React from 'react';
import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Animation, IAnimation } from '../Animation/Animation';
import { ISideBarItemStyled } from './SideBarItemStyled';

export interface ISideBarItem extends IAnimation {
    /** Scroll position in percents */
    scrollPercent: number;
    /** Index determines item position in side bar */
    index: number;
}

// FIXME: move to global constants, could be reused
const itemsCount = 3;
const defaultPercentage = 100 / itemsCount; // 33%
const scaleConversionPercent = 100 / defaultPercentage; // 3%
const scalePercentageStep = 100 / itemsCount; // 33%
const animationPercentageStep = (100 - defaultPercentage) / itemsCount; // 22%

/**
 * Renders items for side bar
 * Items are {@link Animation }, so they require component props to be passed
 * Animation and scaling works automatically, only itemsCount in constants should be changed
 */
export const SideBarItem: React.SFC<ISideBarItem> = ({ index, scrollPercent, width, contentType, className }: ISideBarItem) => {
    /** Defines if animation should be paused */
    const [pause, setPause] = useState(true);
    /** Defines item scaling percentage */
    const [scale, setScale] = useState(index === 0 ? 100 : 0);

    /** Determines which animation should be played */
    const handleItemAnimation = () => {
        const isFirst = index === 0;
        const isLast = index === itemsCount - 1;
        const shouldFirstItemBeAnimated = scrollPercent <= animationPercentageStep + defaultPercentage;
        const shouldLastItemBeAnimated = scrollPercent >= 100 - animationPercentageStep;
        const shouldRestItemsBeAnimated =
            scrollPercent >= index * animationPercentageStep + defaultPercentage &&
            scrollPercent <= (index + 1) * animationPercentageStep + defaultPercentage;

        const shouldBePaused = !(
            (isFirst && shouldFirstItemBeAnimated) ||
            (isLast && shouldLastItemBeAnimated) ||
            (!isFirst && !isLast && shouldRestItemsBeAnimated)
        );
        setPause(shouldBePaused);
    };

    /** Determines scale percentage for items(usually two items will be scaled) */
    const handleItemScaling = () => {
        const isFirst = index === 0;
        const isLast = index === itemsCount - 1;

        if (isFirst && scrollPercent <= defaultPercentage + scalePercentageStep) {
            setScale(getScalePercent(scalePercentageStep * 2 - scrollPercent));
        } else if (isLast && scrollPercent >= 100 - scalePercentageStep) {
            setScale(getScalePercent(scrollPercent - 100 + scalePercentageStep));
        } else if (!isFirst && !isLast) {
            /** Scaling limits for a middle items */
            const bottomLimit = scalePercentageStep * index;
            const middleLimit = scalePercentageStep * (index + 1);
            const topLimit = scalePercentageStep * (index + 2);
            /** above bottom limit && under middle limit */
            if (scrollPercent >= bottomLimit && scrollPercent <= middleLimit) {
                setScale(getScalePercent(scrollPercent - scalePercentageStep));
            }
            /** above middle limit && under top limit */
            if (scrollPercent >= middleLimit && scrollPercent <= topLimit) {
                setScale(getScalePercent(scalePercentageStep * (index + 2) - scrollPercent));
            }
        }
    };

    /**
     * Converts 1-33% to 1-100% scale
     * @param value
     */
    const getScalePercent = (value: number) => scaleConversionPercent * value;

    /** React hooks, that allows change state on scrollPercent change */
    useEffect(handleItemAnimation, [scrollPercent]);
    useEffect(handleItemScaling, [scrollPercent]);

    /** Scrolls to item section */
    const onItemClicked = (e: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
        const sectionCoordinates = window.innerHeight * e.currentTarget.value;
        window.scrollTo({ behavior: 'smooth', top: sectionCoordinates });
    };

    return (
        <ISideBarItemStyled percent={scale} className={className}>
            <li onClick={onItemClicked} value={index}>
                <Animation width={width} contentType={contentType} loop pause={pause} />
            </li>
        </ISideBarItemStyled>
    );
};

export const SideBarItemStyled = styled.div``;
