import styled from 'styled-components';
import { ITheme } from 'styles/theme';

export interface ISideBarItemStyled {
    theme: ITheme;
    /** Scaling percent */
    percent: number;
}

/** Wrapper that scales side bar item(max scale = 1.5) */
export const ISideBarItemStyled = styled.div`
    /* FIXME: styled components warning */
    transform: scale(${({ percent }: ISideBarItemStyled) => 1 + percent * 0.005});
`;
