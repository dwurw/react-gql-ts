import { SectionStyled } from 'components/SectionStyled';
import * as React from 'react';
import { AboutMeStyled } from './AboutMeStyled';

const AboutMe: React.SFC = () => {
    return (
        <SectionStyled>
            <AboutMeStyled className="center">AboutMe</AboutMeStyled>
        </SectionStyled>
    );
};

export default AboutMe;
