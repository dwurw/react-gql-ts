import { createGlobalStyle } from 'styled-components';
import { tachyons } from './micro-tachyons';

export const GlobalStyle = createGlobalStyle`
  ${tachyons}

  html {
    width: 100%;

    position: absolute;

    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    font-size: 2.5vh;

    background:#3490dc;

    ::-webkit-scrollbar { 
    display: none; 
  }
}


  body {
    margin:0;

    width: 90%;

    position: relative;

    text-align: center;

    display: flex;
    flex-direction: column;

    overscroll-behavior: none;
  }

  .center{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
