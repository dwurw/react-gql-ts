import baseStyled, { ThemedStyledInterface } from "styled-components";

export const theme = {
  colors: {
    transparent: "transparent",
    gray: "#CACACA",
    red: "#CD0000",
    black: "#000000",    
  }
};

export type ITheme = typeof theme;
export const Theme = baseStyled as ThemedStyledInterface<ITheme>;
