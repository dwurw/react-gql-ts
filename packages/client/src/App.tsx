import * as React from 'react';
import { lazy, Suspense } from 'react';
import { Header, SideBar } from './components';
/** Lazy loading for the second and the third sections */
const AboutMe = lazy(() => import('./components/AboutMe/AboutMe'));
const PetProject = lazy(() => import('./components/PetProject/PetProject'));

const App: React.SFC = () => {
    return (
        <>
            <SideBar />
            <Header />
            <Suspense fallback={null}>
                <AboutMe />
                <PetProject />
            </Suspense>
        </>
    );
};

export default App;
