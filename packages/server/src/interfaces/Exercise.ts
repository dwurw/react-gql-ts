import {prop} from 'typegoose';

export class Exercise {
  @prop()
  id: string;
  @prop()
  title: string;
  @prop()
  description?: string;
}
