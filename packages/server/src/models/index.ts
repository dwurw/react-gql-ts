import {Typegoose} from 'typegoose';
import {Exercise} from '../interfaces';

export const ExerciseModel = new Typegoose().getModelForClass(Exercise);
