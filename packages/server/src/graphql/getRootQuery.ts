import {GraphQLObjectType} from 'graphql';
import {ExerciseService} from './queries';

export function getRootQuery() {
  /** Injecting  all services */
  const exerciseService = new ExerciseService();

  const fields = {
    getExercise: exerciseService.getExercise(),
    getExercises: exerciseService.getExercises(),
  };

  const queryObject = new GraphQLObjectType({
    name: 'Query',
    fields: () => fields,
  });

  return queryObject;
}
