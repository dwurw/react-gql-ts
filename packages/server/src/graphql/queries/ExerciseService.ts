import { GraphQLString, GraphQLList } from "graphql";
import {ExerciseGQL} from "../types/ExerciseGQL";
import {ExerciseModel} from "../../models";

export class ExerciseService {
  public getExercise() {
    return {
      type: ExerciseGQL,
      args: {
        id: {type: GraphQLString},
      },
      resolve(args) {
        return ExerciseModel.findById(args.id);
      },
    };
  }

  public getExercises() {
    return {
        type: new GraphQLList(ExerciseGQL),
        resolve() {
          return ExerciseModel.find({});
        }
    };
  }
}
