import {GraphQLObjectType, GraphQLString} from 'graphql';

const exerciseField = {
  id: {type: GraphQLString},
  title: {type: GraphQLString},
  description: {type: GraphQLString},
};
export const ExerciseGQL = new GraphQLObjectType({
  name: 'Exercise',
  fields: () => exerciseField,
});
