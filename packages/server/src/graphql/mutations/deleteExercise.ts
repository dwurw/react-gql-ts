import { GraphQLString, GraphQLNonNull } from "graphql";

import {ExerciseGQL} from "../types";
import {ExerciseModel} from "../../models";

export default {
  type: User,
  args: {
    id: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve(parentValue, args) {
    return UserModel.findByIdAndRemove(args.id);
  }
};
