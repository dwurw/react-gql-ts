import { GraphQLString, GraphQLNonNull } from "graphql";

import {ExerciseGQL} from "../types";
import {ExerciseModel} from "../../models";

export default {
  type: ExerciseGQL,
  args: {
    id: { type: GraphQLString },
    nickname: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    dob: { type: new GraphQLNonNull(GraphQLString) },
    superability: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve(parentValue, args, context) {
    let user = new ExerciseModel({
      nickname: args.nickname,
      name: args.name,
      dob: args.dob,
      superability: args.superability
    });
    return user.save();
  }
};
