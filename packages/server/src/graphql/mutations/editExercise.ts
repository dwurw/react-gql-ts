import { GraphQLString, GraphQLNonNull, GraphQLBoolean } from "graphql";

import {ExerciseGQL} from "../types";
import {ExerciseModel} from "../../models";

export default {
  type: ExerciseGQL,
  args: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    nickname: { type: GraphQLString },
    name: { type: GraphQLString },
    dob: { type: GraphQLString },
    superability: { type: GraphQLString }
  },
  resolve(parentValue, args) {
    let exercise = {};
    if (args.nickname !== undefined) {
      exercise.nickname = args.nickname;
    }
    if (args.name !== undefined) {
      exercise.name = args.name;
    }
    if (args.dob !== undefined) {
      exercise.dob = args.dob;
    }
    if (args.superability !== undefined) {
      exercise.superability = args.superability;
    }

    return ExerciseModel.findByIdAndUpdate(args.id, {
      $set: user
    });
  }
};
