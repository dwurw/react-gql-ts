import {GraphQLSchema} from 'graphql';

import mutation from './getRootMutation';
import {getRootQuery} from './getRootQuery';

export default new GraphQLSchema({
  // mutation,
  query: getRootQuery(),
});
