import {ApolloServer} from 'apollo-server';
import mongoose from 'mongoose';
import schema from './graphql/rootSchema';

const server = new ApolloServer({schema});

const mongo = 'mongodb://oxi:super1SecretPassword-_-@ds147180.mlab.com:47180/todo-gql';

/**
 *  Connects to mongo
 *  and starts Apollo server using gql
 */
const startServer = async () => {
  try {
    await mongoose.connect(mongo, {useNewUrlParser: true});
    console.log('connected to mongo-db');
    const info = await server.listen();
    console.log(`🚀 server ready at ${info.url}`);
  } catch (error) {
    console.log(error);
  }
};

startServer();
